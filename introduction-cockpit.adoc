== Web Console 

=== How the Web Console starts

The RHEL Web Console, (project name: Cockpit), is now installed by default in RHEL8. It also carries many improvements since RHEL7. Of particular importance to this lab is that it includes an extra web interface for building virtual machine images.

Cockpit can be used as a simple way for less technical people to administer
RHEL servers. Let's look at cockpit now.

    root@workstation-GUID: systemctl status cockpit
            ● cockpit.service - Cockpit Web Service
       Loaded: loaded (/usr/lib/systemd/system/cockpit.service; static; vendor preset: disabled)
       Active: inactive (dead)
         Docs: man:cockpit-ws(8)

You may need to press **q** to quit this view and return to the shell.

Notice that cockpit is installed and by default on RHEL8, but not yet started. It is only
started (by systemd) when you first connect to it though, which means that CPU
and RAM are not being wasted when it is not being used.

[NOTE]
If your security configuration or preferences are not to use cockpit in
production, it can be disabled and uninstalled like any other RHEL service.
Azure images can still be built from the command line as we'll see later in
this lab.

[#connect-cockpit]
=== Connect to the web console

Access your lab machine from the cockpit URL on the LAB instructions page, and get to the login
screen;

image::labImages/cockpitUrl.png[]

You will need to accept the TLS security warning to continue.

This should open a page like this;

image::labImages/cockpitLogin.png[]

Before you login, go back to the console and see that cockpit was automatically
started; 

----
root@workstation-GUID: systemctl status cockpit
    ● cockpit.service - Cockpit Web Service
       Loaded: loaded (/usr/lib/systemd/system/cockpit.service; static; vendor preset: disabled)
       Active: active (running) since Tue 2019-04-30 15:51:41 EDT; 33s ago <1>
         Docs: man:cockpit-ws(8)
----
<1> Cockpit is now started.

If you do not use Cockpit for a little while, the service will automatically
stop, freeing up system resources.

You can now login with the credentials below; 

[cols="1,3"]
|===
| Username | *root* 
| Password | *r3dh4t1!* 
|===

=== Spend some time exploring the Web Console

Now that you have logged into the Web Console (cockpit), if you have not used it before, take some time to explore the web interface and familiarize yourself with it.

Here are some ideas to look through; 

. **Can you see the chart of user and kernel CPU usage?**
.. Explore the CPU utilization on the "system" tab. 
. **How many warning logs were there recently across all services? **
.. Explore the system logs in the "logs" tab. 
. **Can you see if the "lab-user" account have access to build images?**
.. Explore the user accounts in the "accounts" tab. 

=== Use the web terminal

You'll notice that the last option in the sidebar is the Cockpit web terminal. This works very much like being logged into the system via SSH, or having physical access to the machine.

This terminal is fully functional, and many traditional keyboard shortcuts work.
All of the new enhanced RHEL8 session recording and logging also apply to this
web terminal too.

image::labImages/cockpitTerminal.png[]

**You can now use the Cockpit web terminal as an option to complete the rest of the
lab whenever terminal access is needed**. You can also use the SSH session you already have open - whatever you prefer.
